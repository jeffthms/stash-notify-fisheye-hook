package com.jeffthms.stash.post.notifyfisheye;

import com.atlassian.stash.hook.repository.*;
import com.atlassian.stash.repository.*;
import com.atlassian.stash.setting.*;

import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.util.Collection;

public class NotifyFisheye implements AsyncPostReceiveRepositoryHook, RepositorySettingsValidator {
	@Override
	public void postReceive(RepositoryHookContext context, Collection<RefChange> refChanges) {
		String url = context.getSettings().getString("url");
		String apiKey = context.getSettings().getString("apiKey");

		try {
			scanRepository(url, apiKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void validate(Settings settings, SettingsValidationErrors errors, Repository repository) {
		if (settings.getString("url", "").isEmpty()) {
			errors.addFieldError("url", "URL for Fisheye server is required.");
		}
		if (settings.getString("apiKey", "").isEmpty()) {
			errors.addFieldError("apiKey", "API token is required.");
		}
	}

	private void scanRepository(String request, String apiKey) throws IOException {
		URL url = new URL(request);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		connection.setRequestMethod("POST");
		connection.setRequestProperty("X-Api-Key", apiKey);
		connection.getInputStream().close();
	}
}